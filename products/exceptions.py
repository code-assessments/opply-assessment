

from urllib.parse import ParseResultBytes


class ProductNotFoundException(Exception):
    pass


class ProductOutOfStockException(Exception):
    pass


class NotEnoughStockException(Exception):
    pass
