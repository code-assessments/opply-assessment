from django.contrib import admin

from products.models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("uuid", "name", "id", "price", "quantity_in_stock", "created_at", "updated_at")
    list_filter = ("created_at", "updated_at")
    search_fields = ("uuid", "name", "id")
