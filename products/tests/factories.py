import factory
from factory.fuzzy import FuzzyDecimal
from products.models import Product


class ProductFactory(factory.django.DjangoModelFactory):
    id = factory.Sequence(lambda n: int(n + 1))
    name = factory.Faker("first_name")
    price = FuzzyDecimal(low=0.1, high=60000.0, precision=2)

    class Meta:
        model = Product