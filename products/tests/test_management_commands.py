import json
from io import StringIO
from unittest import mock

from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase

from products.models import Product


class LoadProductsCommandTestCase(TestCase):
    def setUp(self):
        self.invalid_test_data = json.dumps(
            {
                "id": -1,
                "name": "blah",
                "price": "10.00",
                "quantity_in_stock": 32,
            }
        )
        self.valid_test_data = json.dumps(
            [
                {
                    "id": 12345,
                    "name": "this is a test",
                    "price": "10.00",
                    "quantity_in_stock": 32,
                }
            ]
        )

    def test_file_does_not_exist_raises_command_error(self):
        with self.assertRaises(CommandError):
            with mock.patch("builtins.open", mock.mock_open(read_data="")) as mocked_file:
                mocked_file.side_effect = FileNotFoundError
                call_command("load_products", *["test.json"], **{"stdout": StringIO()})

    def test_file_contains_invalid_json_raises_command_error(self):
        with self.assertRaises(CommandError):
            with mock.patch("builtins.open", mock.mock_open(read_data="12345")) as mocked_file:
                mocked_file.side_effect = json.JSONDecodeError(msg="test", doc="test", pos=0)
                call_command("load_products", *["test.json"], **{"stdout": StringIO()})

    def test_file_contains_invalid_data_raises_command_error(self):
        with self.assertRaises(CommandError):
            with mock.patch("builtins.open", mock.mock_open(read_data=self.invalid_test_data)) as mocked_file:
                call_command("load_products", *["test.json"], **{"stdout": StringIO()})

    def test_valid_file_creates_instances(self):
        with mock.patch("builtins.open", mock.mock_open(read_data=self.valid_test_data)) as mocked_file:
            call_command("load_products", *["test.json"], **{"stdout": StringIO()})

        self.assertEqual(Product.objects.all().count(), 1)
