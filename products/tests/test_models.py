from django.test import TestCase

from products.tests.factories import ProductFactory


class ProductTestCase(TestCase):

    def test_str_returns_expected_string(self):
        expected_str = "test_product"
        instance = ProductFactory(name=expected_str)
        self.assertEqual(str(instance), expected_str)
    
    def test_is_in_stock_returns_true_if_quantity_in_stock_greater_than_zero(self):
        instance = ProductFactory(quantity_in_stock=1)
        self.assertTrue(instance.is_in_stock)