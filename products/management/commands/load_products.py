from django.core.management.base import BaseCommand, CommandError, CommandParser
import json

from api.v1.serializers.product import ProductSerializer

DATA_FILE_ARG_NAME = "data_file"


class Command(BaseCommand):
    help = "Load json file of sample products into the db"

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(DATA_FILE_ARG_NAME, nargs="+", type=str)
    
    def handle(self, *args, **options):
        data_file_name = options[DATA_FILE_ARG_NAME][0]
        try:
            with open(data_file_name) as json_product_file:
                product_data = json.load(json_product_file)
        except FileNotFoundError:
            raise CommandError(f"Could not find file {data_file_name}")
        except json.JSONDecodeError:
            raise CommandError(f"Could not parse json file {data_file_name}")

        serializer = ProductSerializer(data=product_data, many=True)

        if not serializer.is_valid():
            raise CommandError(f"Invalid json data in file {serializer.errors}")

        instances = serializer.save()
        self.stdout.write(
            self.style.SUCCESS(f"Successfully added {len(instances)} products to DB")
        )
