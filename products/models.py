from django.db import models

from common.models import ProductStoreBaseModel


class Product(ProductStoreBaseModel):
    """
    A product in the store.
    NOTE: primary key is UUID on ProductStoreBaseModel NOT the id.
    ID implemented as a unique integer field as defined in brief.
    """

    id = models.PositiveSmallIntegerField(unique=True)
    name = models.CharField(max_length=120)
    price = models.DecimalField(max_digits=19, decimal_places=2)
    quantity_in_stock = models.PositiveSmallIntegerField(default=0)

    def __str__(self) -> str:
        return self.name
    
    @property
    def is_in_stock(self) -> bool:
        return self.quantity_in_stock > 0
