from itertools import product
from django.db import models

from common.models import ProductStoreBaseModel


class Order(ProductStoreBaseModel):
    """
    An order a user can make for products.
    Orders are made up of OrderLines so one order can contain multiple products.
    """

    # set null on delete to retain order history for products if a user leaves the plaform
    user = models.ForeignKey(
        "accounts.User",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="orders",
    )


class OrderLine(ProductStoreBaseModel):
    """
    Makes up part of an order for a given product.
    Stores the product, quantity and total cost of purchasing.
    """

    order = models.ForeignKey(
        "orders.Order",
        on_delete=models.CASCADE,
        related_name="order_lines",
    )

    # set null on delete to retain history of _something_ being ordered for user even if product is removed
    product = models.ForeignKey(
        "products.Product",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="order_lines",
    )
    quantity = models.PositiveIntegerField(default=1)
    total_price = models.DecimalField(null=True, blank=True, max_digits=19, decimal_places=2)

    def __str__(self) -> str:
        return f"{self.quantity} x {self.product} (£{self.total_price})"