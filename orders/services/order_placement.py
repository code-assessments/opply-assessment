from django.db.models import When, Value, F, Case, IntegerField
from django.db import transaction
from orders.models import Order, OrderLine
from products.exceptions import NotEnoughStockException, ProductNotFoundException, ProductOutOfStockException
from products.models import Product


def place_order(order_line_data, user):
    """
    Contains logic to place the order.
    NOTE: logic is in atomic block and locks relevant product rows to avoid race conditions.
    """

    product_uuids = order_line_data.keys()
    with transaction.atomic():

        products_for_order = Product.objects.select_for_update().filter(uuid__in=product_uuids)
        num_products = len(products_for_order)

        # if we have more uuids than products, we know some weren't found - raise exception
        if num_products != len(product_uuids):
            raise ProductNotFoundException("One or more products could not be found")
        
        # if some products are out of stock raise exception
        if products_for_order.filter(quantity_in_stock=0).exists():
            raise ProductOutOfStockException("One or more products are out of stock")

        # generate when expressions to annotate products queryset with the amount for each orderline
        requested_quanitity_when_expressions = [
            When(uuid=uuid, then=Value(requested_quantity)) for uuid, requested_quantity in order_line_data.items()
        ]

        # calculate values for order on the queryset with annotations rather than iterating for handling large
        # numbers of orderlines. Also annotating with the uuid of the product for convenience when
        # creating the OrderLine instances in bulk later.
        # this also means updating the products with the new remaining stock values can be done in one update() call
        products_for_order = products_for_order.annotate(
            quantity=Case(
                *requested_quanitity_when_expressions,
                output_field=IntegerField(),
                default=Value(0),
            ),
            total_price=F("quantity") * F("price"),
            remaining_stock=F("quantity_in_stock") - F("quantity"),
            product_uuid=F("uuid"),
        ).order_by()

        if products_for_order.filter(remaining_stock__lt=0).exists():
            raise NotEnoughStockException("Not enough stock for one or more products in order")
        
        # create the order instance as this is atomic, if orderline creation fails this will be rolled back
        order = Order.objects.create(user=user)
        order_line_creation_data = products_for_order.values("product_uuid", "quantity", "total_price")

        order_lines = []
        for line_data in order_line_creation_data:
            new_line = OrderLine(
                order=order,
                total_price=line_data.get("total_price"),
                quantity=line_data.get("quantity"),
            )
            new_line.product_id = line_data.get("product_uuid")
            order_lines.append(new_line)
        
        OrderLine.objects.bulk_create(order_lines, batch_size=1000)

    # dont forget to update the products with new stock amount new the order has been successfully created
    products_for_order.update(quantity_in_stock=F("remaining_stock"))
    return order