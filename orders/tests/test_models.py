from django.test import TestCase

from orders.tests.factories import OrderFactory, OrderLineFactory


class OrderTestCase(TestCase):
    def test_can_create_in_db(self):
        instance = OrderFactory()
        self.assertIsNotNone(instance.pk)
    

class OrderLineTestCase(TestCase):
    def test_str_returns_expected_string(self):
        instance = OrderLineFactory(quantity=3, total_price=40)
        self.assertEqual(
            str(instance),
            f"{instance.quantity} x {instance.product} (£{instance.total_price})",
        )
