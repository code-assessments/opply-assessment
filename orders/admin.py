from django.contrib import admin

from orders.models import OrderLine, Order



class OrderLineInline(admin.TabularInline):
    model = OrderLine
    raw_id_fields = ("product",)
    extra = 0


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("uuid", "user", "created_at", "updated_at")
    list_select_related = ("user",)
    list_filter = ("created_at", "updated_at")
    search_fields = ("uuid", "user__uuid", "user__email", "user__username")
    raw_id_fields = ("user",)
    inlines = (OrderLineInline,)


@admin.register(OrderLine)
class OrderLineAdmin(admin.ModelAdmin):
    list_display = ("uuid", "order", "product", "quantity", "total_price")
    list_select_related = ("order", "product")
    list_filter = ("created_at", "updated_at")
    search_fields = (
        "uuid",
        "order__uuid",
        "order__user__uuid",
        "order__user__email",
        "order__user__username",
        "product__uuid",
        "product__name",
    )
    raw_id_fields = ("order", "product")
