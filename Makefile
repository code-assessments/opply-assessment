docker-run:
	docker-compose -f docker-compose-dev.yml up

docker-bash:
	docker exec -it productstore_web /bin/bash

create-admin:
	python3 manage.py shell -c "from django.contrib.auth import get_user_model; User = get_user_model(); admin = User.objects.create_superuser(username='admin', password='password'); from rest_framework.authtoken.models import Token; Token.objects.create(user=admin);"

test:
	python3 ./manage.py test

test-coverage:
	coverage run --source="." manage.py test
	coverage report -m

sort-imports:
	isort .

format-black:
	black .

formatting: sort-imports format-black

drop-local-db:
	dropdb productsore --if-exists

reset-db: drop-local-db
	createdb productsore
	python manage.py migrate
