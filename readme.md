# CODE ASSESSMENT SUBMISSION (richard smith)

## Intro

Thank you for taking the time to check out my code assessment submission. This project uses django with a postgres db to store products and orders that a user makes. Users can view products, place orders and view their orders view a rest api using the django rest framework.

---

## Set Up

To get this up and running, just create a `.env` file in the project root with the following values:

```
export SECRET_KEY=<SOME VERY SECRET STRING>
export POSTGRES_DB=productstore
export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=postgres
export POSTGRES_HOST=db
export POSTGRES_PORT=5432
```

Then using the provided `Makefile` run `make docker-run`. This will spin up two docker containers, one for the database and one for the web app using the local settings for debug. The web app waits for a successful healthcheck on the `db` container to make sure the db is read to accept data before django starts trying to use it, so it may take a couple of seconds to start up. Once up you should see that the webapp collects static files, runs migrations and starts the dev server.

Once running you should be able to see the admin at `http://0.0.0.0:8000/admin/`.

To create an admin user, exec into the web app container using `make docker-bash`, then `make create-admin` (not to be run in production). This will create you a superuser account with username `admin` and password `password`.

I have also prepared some sample data in `sample_data.json` you can load this into the DB using the management command `./manage.py load_products sample_data.json`.

---

## Using the API

To authenticate using the API send a post request with your username and password. For example if using the generated admin account:

```
curl -X POST http://0.0.0.0:8000/api/v1/auth/obtain-token -d '{"username": "admin", "password": "password"}' -H "Content-Type: application/json"
```

If successful you should get your API token returned in the response.

To see a paginated list of the products in the DB, send a get request to the `api/v1/products/` endpoint with your token in the `Authorization` header:

```
curl -X GET http://0.0.0.0:8000/api/v1/products/ -H "Authorization: Token <YOUR_TOKEN_HERE>" -H "Content-Type: application/json"
```

> NOTE: while all products do have an ID field as required by the brief , all objects in the product store use a uuid for the primary key. This is so in production the project would be less vulnerable to data size/velocity being discovered.

To get the details of a specific product, grab a uuid from the list endpoint and send a get request to `api/v1/products/<product_uuid>/` with your token in the `Authorization` header:

```
curl -X GET http://0.0.0.0:8000/api/v1/products/<PRODUCT_UUID>/ -H "Authorization: Token <YOUR_TOKEN_HERE>" -H "Content-Type: application/json"
```

To veiw all the orders you have placed, send a get request to `api/v1/orders/` with your token in the `Authorization` header. The  results will be filtered to just show the orders for the authenticated user:

```
curl -X GET http://0.0.0.0:8000/api/v1/orders/ -H "Authorization: Token <YOUR_TOKEN_HERE>" -H "Content-Type: application/json"
```

To place an order, send a post request to `api/v1/orders/` with your token in the `Authorization` header. The request body should be a json object called `order_lines` containing product uuid as the key and a positive integer as the value. You can order multiple products at a time by adding more uuid: quantity pairs into the request body. For Example:

```curl -X POST http://0.0.0.0:8000/api/v1/orders/ -H "Authorization: Token <YOUR_TOKEN_HERE>" -H "Content-Type: application/json" -d '{"order_lines": {"<PRODUCT_UUID>": 10}}'```

Sending a non-uuid key in the request will return a 400 response.

If any product in the order lines is out of stock, the order will fail with a 400 response. Also, if ay order would take a product to a stock value lower than 0 the order will fail with a status of 400. If any of the products in the order cannot be found the order will fail with a status of 404. Thw whole order fails to avoid having orders being partially completed.

After placing an order hit either the product list or detail endpoint to see the quantity in stock has been reduced.

NOTE: to avoid race conditions, the product rows in the db related to the order lines are locked. Requests trying to access at the same time will wait until the lock is released.

---

## Future Work

Things I would have liked to do with more time:
- more testing on performance. I have used querysets for handling the order lines to avoid iteration over potentially very large numbers of orderlines/products but it would be good to test this futther.
- Filtering in the api (filtering products by stock levels, price)
- Adding sellers, categories, tags etc
- Potentially refactoring the orderlines POST endpoint. Using a simple uuid:quantity object felt like the simplest way to get the order data into the api supporting multiple products (I did try an array for a while but it got quite messy). There is probably a cleaner, more efficient way of doing this - I'd like to find it!


## A Note On Deployment
To deploy this for example on AWS, for simplicity we could use EC2 + Elastic Beanstalk with an Amazon RDS for the postgres DB. The static/media files would need to be configured to use S3 so products could have images.