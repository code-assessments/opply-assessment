import uuid

from django.db import models
from django.utils import timezone


class ProductStoreBaseModel(models.Model):
    """
    Base model implementing fields and logic common across all project models
    UUIDs are used as good practice for APIs to avoid revealing data size/velocity.
    """

    uuid = models.UUIDField(
        default=uuid.uuid4, unique=True, editable=False, primary_key=True
    )
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ("-created_at",)

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        return super().save(*args, **kwargs)
