from datetime import datetime
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.test import APITestCase

from accounts.models import User
from api.v1.constants import DATETIME_FORMAT
from products.tests.factories import ProductFactory
from productstore.settings.base import API_PAGE_SIZE


class ProductViewSetTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="test_person", password="very_secure")
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.token.key}")
    
    def test_unauthenticated_request_returns_401(self):
        self.client.credentials()
        response = self.client.get(reverse("api:v1:product-list"))
        self.assertEqual(response.status_code, HTTP_401_UNAUTHORIZED)
    
    def test_list_endpoint_returns_paginated_results(self):
        number_of_instances = API_PAGE_SIZE * 3
        ProductFactory.create_batch(size=number_of_instances)
        response = self.client.get(reverse("api:v1:product-list"))
        data = response.json()
        self.assertEqual(len(data.get("results")), API_PAGE_SIZE)
        self.assertEqual(data.get("count"), number_of_instances)

    def test_detail_endpoint_returns_expected_data(self):
        instance = ProductFactory()
        response = self.client.get(reverse("api:v1:product-detail", kwargs={"pk": instance.pk}))
        expected = {
            "id": int(instance.id),
            "name": instance.name,
            "price": str(instance.price),
            "quantity_in_stock": instance.quantity_in_stock,
            "uuid": str(instance.uuid),
            "created_at": datetime.strftime(instance.created_at, DATETIME_FORMAT),
            "updated_at": datetime.strftime(instance.updated_at, DATETIME_FORMAT),
        }
        self.assertEqual(response.json(), expected)