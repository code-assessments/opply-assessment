import uuid
from decimal import Decimal
from datetime import datetime
from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from accounts.models import User
from api.v1.constants import DATETIME_FORMAT
from orders.models import Order, OrderLine
from orders.tests.factories import OrderFactory, OrderLineFactory
from products.tests.factories import ProductFactory
from productstore.settings.base import API_PAGE_SIZE


class OrderViewSetTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="mr_test", password="very_secure")
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.token.key}")

    def test_list_returns_paginated_list_of_orders_for_only_authenticated_user(self):
        num_instances = API_PAGE_SIZE * 3
        OrderFactory.create_batch(user=self.user, size=num_instances)
        OrderFactory.create_batch(size=num_instances)
        response = self.client.get(reverse("api:v1:order-list"))
        data = response.json()

        self.assertEqual(len(data.get("results")), API_PAGE_SIZE)
        self.assertEqual(data.get("count"), num_instances)
    
    def test_detail_endpoint_returns_expected_data(self):
        instance = OrderFactory(user=self.user)
        order_line = OrderLineFactory(order=instance, total_price=Decimal("3"))
        response = self.client.get(
            reverse("api:v1:order-detail", kwargs={"pk": instance.uuid})
        )
        data = response.json()
        expected = {
            "created_at": datetime.strftime(instance.created_at, DATETIME_FORMAT),
            "order_lines": [
                {
                    "order": str(instance.uuid),
                    "total_price": "3.00",
                    "product": str(order_line.product.uuid),
                    "quantity": 1,
                    "updated_at": datetime.strftime(order_line.updated_at, DATETIME_FORMAT),
                    "created_at": datetime.strftime(order_line.created_at, DATETIME_FORMAT),
                    "uuid": str(order_line.uuid),
                }
            ],
            "updated_at": datetime.strftime(instance.updated_at, DATETIME_FORMAT),
            "user": str(self.user.uuid),
            "uuid": str(instance.uuid),
        }

        self.assertEqual(data, expected)

    def test_detail_returns_404_for_unrelated_order(self):
        instance = OrderFactory()
        response = self.client.get(
            reverse("api:v1:order-detail", kwargs={"pk": instance.uuid})
        )

        self.assertEqual(response.status_code, 404)
    
    def test_order_for_out_of_stock_product_returns_400(self):
        instance = ProductFactory(quantity_in_stock=0)
        response = self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {str(instance.uuid): 4}},
            format="json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), "One or more products are out of stock")

    def test_order_with_invalid_uuid_returns_400(self):
        instance = ProductFactory(quantity_in_stock=0)
        response = self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {"BLAHHH": 4}},
            format="json",
        )

        self.assertEqual(response.status_code, 400)

    def test_order_for_more_product_than_is_in_stock_returns_400(self):
        instance = ProductFactory(quantity_in_stock=2)
        response = self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {str(instance.uuid): 4}},
            format="json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(), "Not enough stock for one or more products in order"
        )

    def test_invalid_orderlines_do_not_create_order(self):
        instance = ProductFactory(quantity_in_stock=2)
        self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {str(instance.uuid): 4}},
            format="json",
        )

        self.assertEqual(Order.objects.filter(user=self.user).count(), 0)

    def test_invalid_orderlines_do_not_create_order_lines(self):
        instance = ProductFactory(quantity_in_stock=2)
        self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {str(instance.uuid): 4}},
            format="json",
        )

        self.assertEqual(OrderLine.objects.all().count(), 0)

    def test_successful_order_creates_order_in_db(self):
        instance = ProductFactory(quantity_in_stock=20)
        self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {str(instance.uuid): 4}},
            format="json",
        )
        self.assertEqual(Order.objects.filter(user=self.user).count(), 1)
    
    def test_successful_order_creates_orderlines_in_db(self):
        instance = ProductFactory(quantity_in_stock=20)
        self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {str(instance.uuid): 4}},
            format="json",
        )
        self.assertEqual(OrderLine.objects.all().count(), 1)

    def test_successful_order_reduces_quantity_in_stock(self):
        instance = ProductFactory(quantity_in_stock=20)
        another_instance = ProductFactory(quantity_in_stock=2)
        self.client.post(
            reverse("api:v1:order-list"),
            data={
                "order_lines": {str(instance.uuid): 4, str(another_instance.uuid): 1}
            },
            format="json",
        )
        instance.refresh_from_db()
        another_instance.refresh_from_db()
        self.assertEqual(instance.quantity_in_stock, 16)
        self.assertEqual(another_instance.quantity_in_stock, 1)

    def test_unknown_product_returns_404(self):
        response = self.client.post(
            reverse("api:v1:order-list"),
            data={"order_lines": {str(uuid.uuid4()): 4}},
            format="json",
        )

        self.assertEqual(response.status_code, 404)
