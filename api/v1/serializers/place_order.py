import uuid
from rest_framework import serializers
from django.core.validators import MinValueValidator
from rest_framework.exceptions import ValidationError


class PlaceOrderSerializer(serializers.Serializer):
    """
    Serializer to accept a dict of format {product_uuid: quantity}.
    Quantities must be greater than 0 and keys must all be valid uuids.
    Each entry in this dict represents an orderline making up the order.
    """
    order_lines = serializers.DictField(child=serializers.IntegerField(validators=[MinValueValidator(1)]))

    def validate_order_lines(self, data):
        try:
            [uuid.UUID(key) for key in data.keys()]
        except ValueError:
            raise ValidationError("One of more invalid product uuids")
        return data
