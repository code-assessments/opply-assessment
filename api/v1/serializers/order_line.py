from rest_framework import serializers

from orders.models import OrderLine

class OrderLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderLine
        fields = "__all__"
        read_only_fields = ("order",)