from rest_framework import serializers
from api.v1.serializers.order_line import OrderLineSerializer

from orders.models import Order


class OrderSerializer(serializers.ModelSerializer):
    order_lines = OrderLineSerializer(many=True)

    class Meta:
        model = Order
        fields = "__all__"
