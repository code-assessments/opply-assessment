from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED, HTTP_404_NOT_FOUND
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated

from api.v1.serializers.order import OrderSerializer
from api.v1.serializers.place_order import PlaceOrderSerializer
from orders.models import Order
from orders.services.order_placement import place_order
from products.exceptions import (
    NotEnoughStockException,
    ProductNotFoundException,
    ProductOutOfStockException,
)


class OrderViewSet(ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderSerializer

    # still add queryset here as sometimes tools like ReDoc cannot infer the types when using get_queryset
    queryset = Order.objects.none()

    def get_queryset(self):
        """
        Override get_queryset to make sure the authenticated user can only see their own orders.
        Also select/prefetch related fields for better performance on list view.
        """
        return Order.objects.filter(user=self.request.user).select_related("user").prefetch_related("order_lines")

    def create(self, request, *args, **kwargs):
        data = request.data
        serializer = PlaceOrderSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        try:
            order = place_order(serializer.validated_data.get("order_lines"), request.user)
        except ProductNotFoundException as e:
            return Response(status=HTTP_404_NOT_FOUND, data=str(e))
        except (ProductOutOfStockException, NotEnoughStockException) as e:
            return Response(status=HTTP_400_BAD_REQUEST, data=str(e))
        
        return Response(status=HTTP_201_CREATED, data=OrderSerializer(instance=order).data)