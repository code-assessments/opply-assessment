from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated
from api.v1.serializers.product import ProductSerializer

from products.models import Product


class ProductViewSet(ReadOnlyModelViewSet):
    """
    Simple viewset for the Product class providing paginated list and detail endpoints.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
