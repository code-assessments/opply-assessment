from rest_framework import routers
from api.v1.views.products import ProductViewSet
from api.v1.views.orders import OrderViewSet

router = routers.SimpleRouter()

router.register(r"products", ProductViewSet)
router.register(r"orders", OrderViewSet)

v1_router_url_patterns = router.urls