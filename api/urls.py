from django.urls import include, path
from rest_framework.authtoken import views

from api.v1.router import v1_router_url_patterns

app_name = "api"
urlpatterns = [
    path("v1/auth/obtain-token", views.obtain_auth_token, name="obtain-token"),
    path("v1/", include((v1_router_url_patterns, "v1"), namespace="v1")),
]