from django.contrib import admin

from django.contrib.auth.admin import UserAdmin


from accounts.models import User



@admin.register(User)
class CustomUserAadmin(UserAdmin):
    list_display = (
        "uuid",
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_active",
        "is_superuser",
        "date_joined",
    )
    list_filter = ("is_staff", "is_active", "is_superuser", "date_joined", "created_at", "updated_at", )
    search_fields = ("uuid", "username", "email", "first_name", "last_name")