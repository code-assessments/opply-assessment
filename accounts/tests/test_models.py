from django.contrib.auth import get_user_model
from django.test import TestCase


class UserModelTestCase(TestCase):
    def test_can_create_user(self):
        User = get_user_model()
        instance = User.objects.create_user(username="test_user", password="super_safe")
        self.assertIsNotNone(instance.pk)
