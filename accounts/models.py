from django.contrib.auth.models import AbstractUser

from common.models import ProductStoreBaseModel


class User(ProductStoreBaseModel, AbstractUser):
    """
    Custom user model for extending with extra logic/fields. (i.e. future different user types)
    Better to add this at the start of a project even if left blank incase it's needed for the future.
    Adding a custom user model at a later date can be a pain.
    """

    pass
