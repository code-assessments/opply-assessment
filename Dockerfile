# syntax=docker/dockerfile:1
FROM python:3.8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY . /code/
RUN apt-get update && apt-get install libpq-dev && pip3 install --upgrade pip && pip3 install -r requirements/local.txt
